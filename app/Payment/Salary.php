<?php

namespace App\Payment;

use App\Enums\PaymentTypes;
use App\Payment\PaymentType;

class Salary extends PaymentType
{
    // Payment/Salary
    public function monthlyAmount(): int
    {
        // 月薪 = 年薪 / 12
        return $this->employee->salary / 12;
    }

    public function type(): string
    {
        return PaymentTypes::SALARY->value;
    }

    public function amount(): int
    {
        return  $this->employee->salary;
    }
}
