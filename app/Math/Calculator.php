<?php

namespace App\Math;

use DivisionByZeroError;

class Calculator
{
    public function divide(float $n1, float $n2): float
    {
        try {
            return round($n1 / $n2, 2);
        } catch (DivisionByZeroError) {
            return 0;
        }
    }
}
