<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Department;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use App\Http\Resources\EmployeeResource;

class DepartmentEmployeeController extends Controller
{
    public function index(Request $request,Department $department)
    {
        $employees = QueryBuilder::for(Employee::class)
                ->allowedFilters(['full_name', 'job_title', 'email'])
                ->whereBelongsTo($department)
                ->get();

        return EmployeeResource::collection($employees);
    }
}
