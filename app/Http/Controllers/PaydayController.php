<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actions\PaydayAction;

class PaydayController extends Controller
{
     public function __construct(private readonly PaydayAction $payday)
     {

     }

    public function store(Request $request)
    {
       $this->payday->execute();
       return response()->noContent();
    }
}
