<?php
namespace App\Actions;

use App\Models\Employee;

class  PaydayAction{
    public function execute(): void
    {
        foreach (Employee::all() as $employee) {
            $employee->paychecks()->create([
                'net_amount' => $employee->payment_type->monthlyAmount(),
                'payed_at' => now(),
            ]);
        }
    }
}
