<?php

use App\Models\Department;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use function Pest\Laravel\postJson;


//测试部门创建接口是否可以调用成功
it('should create a department', function () {
    // 创建部门接口需要认证
    Sanctum::actingAs(User::factory()->create(), ['*']);

    // 调用 API 接口
    $department = postJson(route('departments.store'), [
        'name' => 'Development',
        'description' => 'Awesome developers across the board!',
    ])->json('data');
    //var_dump($department);

    // 断言响应数据
    expect($department)
        ->name->toBe('Development')
        ->description->toBe('Awesome developers across the board!');

});

//测试部门名称这个请求字段的校验规则
it('should return 422 if name is invalid', function (?string $name) {
    // 需要认证
    Sanctum::actingAs(User::factory()->create(), ['*']);

    // 通过模型工厂先创建一个同名部门
    Department::factory([
        'name' => 'Development'
    ])->create();

    // 通过 assertInvalid 方法断言 name 是否校验不通过
    postJson(route('departments.store'),[
        'name' => $name,
        'description' => 'description',
    ])->assertInvalid(['name']);

})->with([
    // 传递不同值作为 $name 参数进行测试
    '',
    null,
    'Development'
]);
