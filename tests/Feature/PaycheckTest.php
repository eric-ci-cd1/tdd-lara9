<?php

use App\Models\User;
use App\Models\Employee;
use App\Enums\PaymentTypes;
use Laravel\Sanctum\Sanctum;

use function Pest\Laravel\postJson;

// 为工薪员工创建薪资支票
// it('should create paychecks for salary employees', function () {
//     Sanctum::actingAs(User::factory()->create(), ['*']);

//     $employees =  Employee::factory()
//         ->count(2)
//         ->sequence(
//             [
//                 'salary' => 50000 * 100,
//                 'payment_type' => PaymentTypes::SALARY->value
//             ],
//             [
//                 'salary' => 70000 * 100,
//                 'payment_type' => PaymentTypes::SALARY->value
//             ]
//         )
//         ->create();

//     postJson(route('payday.store'))
//         ->assertNoContent();

//     $this->assertDatabaseHas('paychecks', [
//         'employee_id' => $employees[0]->id,
//         'net_amount' => 50000 * 100 / 12,
//     ]);
//     $this->assertDatabaseHas('paychecks', [
//         'employee_id' => $employees[1]->id,
//         'net_amount' => 70000 * 100 / 12,
//     ]);
// });
